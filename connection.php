<?php
$serverName = "localhost";
$username = "root";
$password = "";
$database = "QLSV";

try {
    $con = new PDO("mysql:host=$serverName;dbname=$database", $username, $password);
    $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo "Connection failed" . $e->getMessage();
}
