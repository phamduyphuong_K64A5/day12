DROP DATABASE IF EXISTS QLSV;

CREATE DATABASE QLSV;

USE QLSV;

CREATE TABLE `Student` (
`id` int(11) NOT NULL,
`name` varchar(250) NOT NULL,
`gender` int(1) NOT NULL,
`faculty` char(3) NOT NULL,
`birthday` datetime NOT NULL,
`address` varchar(250) DEFAULT NULL,
`avatar` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
